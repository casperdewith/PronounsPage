# The amazing revolution of <em>niebinarszczyzna</em>

<small>2023-06-07 | [@andrea](/@andrea)</small>

![A bunch of books standing on a table one behind another: our zine “Poczytałosie”, Mason Deaver's “All the best”, Mark Gevisser's “Pink line” and Katarzyna Skrzydłowska-Kalukin's and Joanna Sokolińska's “Mów o mnie ono”](/img-local/blog/polish-books.jpeg)

Every once in a while I get reminded of just how much of what we do is pretty much invisible
for most visitors of Pronouns.page, simply because a big chunk of our work is focused on a language that they don't speak 😅

As you [might know](/history), this project started out as [“zaimki.pl”](https://zaimki.pl),
a website that collects, promotes and educates about different ways to avoid gendering oneself and others
in a highly gendered language – Polish.

But the story doesn't end there. Over the last few years we're seeing big changes in how
nonbinary Polish – or _niebinarna polszczyna_, or _niebinarszczyzna_ – evolves,
gains social acceptance, legitimacy, and popularity among the nonbinary folks themselves.

So I wrote down a summary of what we've been up to on the Polish side of Pronouns.page.
Admittedly, a little bit as a boast – but most importantly I'm hoping to inspire 
current and future activists in other places by showing what kind of initiatives are possible.
Enjoy reading!

{table_of_contents}

## Why is it so hard for nonbinary people to express themselves in Polish? 

Before we get to the main part of the post,
let me start with a TL;DR introduction to how gender works in Polish.
A slightly longer one is available [here](https://zaimki.pl/english), if you're interested,
and if you're not interested at all, feel free to scroll down to the next section – 
just know that gender in Polish is _massively_ complicated.

The thing is, unlike in English where it's (almost) all about the pronouns,
in Polish there's an entire system of noun classification called
[grammatical gender](https://en.wikipedia.org/wiki/Grammatical_gender)
(which is different from human gender, albeit somehow linked).
Every noun has an innate gender, e.g. a table is masculine (“**ten** stół”),
a spoon is feminine (“**ta** łyżka”) and a chair is neuter (“**to** krzesło”) –
there's no real “logic” in it, it's just a grammatical class.
Many other languages have a system like that (German _der/die/das_, French _le/la_, Dutch _de/het_, etc.),
but Polish makes stuff very complicated considering how many other parts of speech must align with the noun's gender 
– as well as with people's personal pronouns…

In Polish you can't say something as simple as “I did” without expressing your gender.
It's either “zrobi**łam**”, if you're using [feminine forms](https://zaimki.pl/ona), or “zrobi**łem**”, if you go with [masculine ones](https://zaimki.pl/on).
For “I'm hungry” – it's either “jestem głodn**a**” or “jestem głodn**y**”.
Basically: it's not just about pronouns in a bunch of grammatical forms, but an entire system of pronouns and suffixes.
And it's not just about people talking _about you_ (3<sup>rd</sup> person) – the grammatical gender that you use influences
things you say _about yourself_ (1<sup>st</sup>) and things that people say _to you_ (2<sup>nd</sup>).
It's hard to speak _normative_ Polish when you're nonbinary.

_How come?_ – you might ask. – _Didn't you just say that there's a third grammatical gender, [neuter](https://zaimki.pl/ono)?_
Well, yes! You could say “zrobi**łom**” and “jestem głodn**e**”.
But here's a thing: although [technically grammatically correct](https://rjp.pan.pl/index.php?option=com_content&view=article&id=317:byom-byo&catid=44&Itemid=208)
and not without some historical use, in practice it's not really being used for humans (rarely for children, but in 3<sup>rd</sup> person only),
and referring to an adult with such forms would be considered by most to be insulting and weird.
Or maybe I should say: it _used to_ be like that? After all, us queers tend to be great at reclaiming slurs
and turning the weird and insulting stuff into something that we wear with pride.
In the recent years the neuter forms seem to have gained a surprising amount of support, representation and understanding
– at least in the leftist circles and amongst the younger generations.

But there are other options too. One might use plural forms, as a kind of [calque](https://en.wikipedia.org/wiki/Calque)
from the English [_they/them_](https://en.pronouns.page/they) and/or extension of historical and regional [_pluralis majestatis_](https://en.wikipedia.org/wiki/Royal_we) 
– although an extra complication here is that Polish has two plurals:
[masculine personal](https://zaimki.pl/oni) (“zrobi**liśmy**”, “głodn**i**”) and [non-masculine](https://zaimki.pl/one) (“zrobi**łyśmy**”, “głodn**e**”).
There's also a system that we might call neopronouns (or rather neogender), called [dukaizmy](https://zaimki.pl/onu),
that takes advantage of the fact that the vowel “u” is not used by any other gendered forms,
so we end up with “zrobi**łum**”, “głodn**u**”.
In writing, people also often use [graphical placeholders](https://zaimki.pl/onx), eg. “zrobi**łxm**” and “głodn**x**”,
or “zrobi**ł_m**” and “głodn**_**”.

And I haven't even started talking about the gendered nouns yet…
Think of “actor – actress” or “fiancé – fiancée” – as opposed to “painer”, “politician”, “worker” etc.
Except in Polish there are hardly any nouns describing a person's job or relationship that _aren't_ gendered
(and also a subject of a heated public debate even when it comes to letting women
describe themselves without [androcentric](https://en.wikipedia.org/wiki/Androcentrism) nouns).
Such nouns don't follow any easy grammar tables, we basically had to create a whole dictionary with thousands of entries!

Believe it on not – that was the TL;DR version… Polish grammatical gender is complicated AF 😅
Making it more inclusive is no easy endeavor. So how do we go about doing that?

## Polish Nonbinary Census

One of our biggest undertakings each year, as well as the biggest research endeavor of this kind in Poland,
is running the [Polish Nonbinary Census](https://zaimki.pl/spis), a survey of the Polish nonbinary language – 
with [2211](https://zaimki.pl/blog/spis-2021), [1816](https://zaimki.pl/blog/spis-2022), and [3022](https://zaimki.pl/blog/spis-2023) answers in the respective years.
It's inspired by Cassian's [Gender Census](https://www.gendercensus.com/).
Even if you don't understand a word of it, you might wanna [open the most recent report](https://zaimki.pl/blog/spis-2023)
and just scroll through the charts, tables and collapsable sections
(even though I automated the generation of many parts of the report, it's still been a _ton_ of work to put it together,
so I might as well show it off 😉).

I can't express how valuable for us is the data that we collect there.
Without it, we'd basically be walking in the dark, always uncertain if we even make any tangible difference,
and if our recommendations, efforts and goals actually reflect the actual usage in the community…

The Census gives us useful information for recommending translators the forms they might wanna use for nonbinary characters.
It helps Polish enbies chose their own pronouns. It gives us solid arguments against claims that “no one uses those forms in real life”.
It's a great tool to measure how much braver the community gets each year about using unconventional language,
and also to pinpoint and tackle the reasons why they don't.
Many people say that through filling out the survey they found out about possibilities they weren't aware of before.
People who don't know many/any other enbies IRL say that just seeing the data makes them feel valid and less lonely.

And for me personally… analysing the Census data gives me confidence that our efforts are worth it.

## Manifesto

Polish-speaking enbies have many options to express themselves now, and sometimes it just seems like there are… too many.
After all, it's a relatively new area of language, still in a state of a linguistic _primordial soup_,
and it touches on many aspects of an already complicated grammar system. It's easy to get overwhelmed!

So we used the knowledge gained from the Census data,
as well as historical sources and expert opinions that we'd found,
we've analysed the trends we see and the linguistic and sociological context in which nonbinary Polish is used,
and we formulated a [Manifesto of _niebinarszczyzna_](https://zaimki.pl/manifest).

We're not being [prescriptive](https://en.wikipedia.org/wiki/Linguistic_prescription) about it,
we're not claiming that we know best how language _should_ or _has to_ be used,
but there's been a need for some kind of guidance on how to navigate the new forms –
so we prepared a document in which we explain why respecting nonbinary identities and our language matters,
and which forms seem to be on the best way to become a norm in the near future and to create a consistent system.

Our Manifesto makes life a bit easier for enbies looking for pronouns for themselves,
or authors and translators looking for pronouns for their characters,
or even just allies trying to make sense of all of it.

## Neutratywy, osobatywy

As I mentioned before, Polish nouns describing people's occupations and relationships are very gendered and not regular.
Think of triples like “barman – barmaid – bartender” – except the concept applies to basically all such nouns, 
plus the second form still keeps stirring some controversies, and also the third form… doesn't exist.

But here's a thing about language: if there's a need to describe something, language will accommodate that need.
After all, helping us express ideas and communicate with each other is literally its main purpose.
When humanity invented printers, lasers, computers or space rockets, we also had to invent the words to describe them.
And when nonbinary identities started becoming less suppressed by the society and the need arose to better describe ourselves,
it became invitable that our language would evolve to fulfil it.
That evolution is happening right in front of our eyes.

[Sybil](/@sybil) came up with a system called [_neutratywy_](https://zaimki.pl/neutratywy)
that combines the relevant roots with endings of nouns that might not describe people,
but are grammatically neuter. For example “autor” (_author_, masc.) can receive the ending and declension of “morze”
(_sea_, neuter) to produce “autorze” (_author_, neuter). It's a brilliant system, in which brand-new words
can accurately describe someone, be easily understood by people hearing them for the first time,
and, thanks to using known suffixes and following known declension patterns, feel like a natural part of the language –
even though they sound very weird at first. 

[Neologisms](https://en.wikipedia.org/wiki/Neologism) are, of course, common in languages,
but adding an entire, vast _category_ of nouns to the vocabulary is quite a rarity.
Our English [dictionary of gender-neutral language](/dictionary) has around a hundred entries, and mostly normative,
while Polish has over five thousand neologisms!
And they're not just art for art's sake – they address a real need of the community and are gaining actual use.

Aside from _neutratywy_, we also proposed less complex noun systems that grammatically match the neopronouns/neogender
and graphical forms. But there's one more concept worth mentioning:

_[Osobatywy](/osobatywy)_ are forms that combine a gender-neutral word “osoba” (_person_) with a descriptor.
Admittedly, they're a bit of a mouthful and might sound clunky,
but they allow to describe a nonbinary person or a person of unknown/irrelevant gender without using non-normative language.
Credits for coming up with this idea go to [Fundacja Trans-Fuzja](https://www.transfuzja.org/),
(but we're also proud of helping popularise it 😉).

## Media appearances and mentions, workshops and lectures

![Screenshot from a live expert panel discussion at TęczUJ](/img-local/blog/tęczuj-panel-ekspercki.jpeg)

If you open [zaimki.pl/media](https://zaimki.pl/media), you'll see a list of our appearances and mentions in various media outlets.
Some of them are, admittedly, tiny, but the list also includes likes of Newsweek Poland,
one of the biggest TV stations in the country (TVN), and even the conservative government propaganda machine (TVP).
Our members have been invited to live interviews, have been giving lectures, leading workshops,
entering into polemics with journalists.

What struck me most about those experiences is how much acceptance and curiosity we've been welcomed with.
Of course conservatives are gonna conserve, but among more open-minded people it's always been
an absolute pleasure to discuss nonbinary identities and gender-neutral language with people ready to learn.

Setting up a tiny, simple website about language, I never would've thought that just a few months later
I'd be showing my face to thousands of people and educating them about a topic so niche that we,
just a bunch of queers with a passion for language, basically became experts in it.
Just scrolling through that list is a bit overwhelming to me.

## Explosion in the number of texts using nonbinary language

![Graph described below](/img-local/blog/korpus-wykres-2021.png)

We collect [a corpus of sources](https://zaimki.pl/korpus) (books, press articles, movies, series, games, songs…)
that use nonbinary language. It's by no means a complete catalogue, nor are the inclusion criteria very strictly defined,
but it's a very useful collection nonetheless. I was curious how did the number of such sources change over time,
so I put it on a chart.

As you can see, until 2018 usages were sparse. But in 2019 an uptick starts, and in 2020 (when zaimki.pl were created)
and 2021 it turns into quite a spike.

By no means am I claiming that it's us who made that happen – after all, the growing acceptance towards nonbinary people
and our increased representation in media (both Polish and translations of foreign texts)
must be responsible for a huge chunk of that spike. But I like to think that our efforts in popularising and normalising
nonbinary language have also contributed to that trend. Maybe we inspired some Polish authors to include
unapologetically nonbinary characters in their literature, and maybe we convinced some Polish translators
to properly represent such characters instead of erasing their identity through language,
as [used to be more common](https://anglica-journal.com/resources/html/article/details?id=207730).

## Books and video games – sensitivity reviews, consultations, credits, auspices

If you head to [zaimki.pl/linki](https://zaimki.pl/linki), you'll see a couple of book covers.
That's because we've been participating in the process of their creation.
Our team members have been doing sensitivity reviews, advising the translators, providing suggestions.
We did what little we could to promote those books, and we're proud to see our logo in the “auspices” section on their covers.

One time we responded to an email, in which a translator was asking a bunch of questions about their project
(without revealing what it is). To our surprise, a few months later someone messaged us, asking if we're aware
that we're mentioned in the closing credits to the game [Bugsnax](https://bugsnax.com/) 🤯

And that's not even the only video game that we've been involved with.
I recently did sensitivity reviews for a big game company for two upcoming titles and an internal DEI guide.
Unfortunately, NDA won't let me say much more, but still… it's all so exciting!

## Terminology

Although the [Dictionary of Queer Terminology](/terminology) exists also in other language versions of Pronouns.page,
there's a bit more to it than just gathering a bunch of terms with flags and definitions.

The idea behind the dictionary was to promote Polish versions of queer terms instead of simply borrowing them from English.
Not that we're against [loanwords](https://en.wikipedia.org/wiki/Loanword) –
after all my own language is basically [Poglish](https://en.wikipedia.org/wiki/Poglish) (or rather Pong-ger-dutch-lish?), so who am I to judge –
but in this context popularising Polish alternatives plays its role in refuting queerphobes' claims
that our identities aren't our own, that we're just blindly following a “trend from the West”.

Many of the terms are translated quite literally, but with a few we had to be a bit more creative –
including for example
[płynnopłciowość](https://zaimki.pl/terminologia#p%C5%82ynnop%C5%82ciowo%C5%9B%C4%87) (genderfluid),
[zmiennopłynność](https://zaimki.pl/terminologia#zmiennop%C5%82ynno%C5%9B%C4%87) (fluidflux)
or [nekronim](https://zaimki.pl/terminologia#nekronim) (deadname).

## Inclusive dictionary

Similarly, the [Dictionary of inclusive language](/inclusive) was initially meant for the Polish website
and only later extended to English. Currently it has three times as many entries as the English version!

## Zine

There was a time when people questioning their gender who were asking in online spaces for materials
that might help them to navigate that journey were primarily getting the following response: learn English.
There was simply not enough places where you could read about other enbies' experiences in Polish.

It's been improving since, of course, but as a part of our contribution to the process
we wanted to create [a zine](https://zaimki.pl/zin) where members of our community could share their thoughts and experiences.
We were open to all (printable) forms of expression, which resulted in a great variety among the entries –
ultimately the zine ended up containing prose, poetry, prose poetry, opinion pieces, drawings,
and even a diagram-story.

![Picture of five copies of the zine. The cover is in the colours of the nonbinary flag, it's divided into rectangles, each with a drawn character; and on the left, vertically there's the title: “Poczytałosie”](/img-local/blog/zine-polish-2022.jpeg)

Fun fact: the layout of the zine was created by my partner – despite them not speaking Polish at all!
And my husband came up with the name for it: “Poczytałosie”, which is a play on words “reading” and “moose”.
Why moose? Because the 2<sup>nd</sup> person past tense verbs form of the neuter grammatical gender
end with -łoś, which happens to… also be a word for “moose”.

The zine is published as a PDF at [zaimki.pl/zin](https://zaimki.pl/zin),
but we've also assigned an ISBN to the publication and printed out a hundred physical copies
that we shared with the authors, editors and members of the Collective – and the [National Library](https://www.bn.org.pl/) too! 

## Our own grammatical plural gender

Yet another [androcentric](https://en.wikipedia.org/wiki/Androcentrism) complication of the Polish grammatical gender system
is that when talking to or about groups of people, one should pick a gender according to some pretty weird rules. 
Groups of women are described with the same forms (eg. “kobiety **były**”) as animals (“psy **były**”) and inanimate objects (“krzesła **były**”) –
but a single man joining their group “upgrades” their grammatical gender to masculine personal forms (eg. “widzowie **byli**”).

So we've decided to come up with a brand new grammatical gender that would be totally gender-neutral
and fine to use by anyone – without having to wonder what gender are members of a given group and which rules to apply.
We've brainstormed a system that's inspired both by normative singular neuter forms,
and by plural pronouns in other Slavic languages; and we ended up with a neat system of [“ona/ich”](https://zaimki.pl/ona/ich).
It's not normative, and admittedly hasn't gained much popularity (yet?),
but we're very proud of it – and of course we use it when describing ourselves as [a Collective](https://zaimki.pl/kolektyw-rjn).

## Unisex names dictionary and data analysis

[Rejestr PESEL](https://obywatel.gov.pl/pl/dokumenty-i-dane-osobowe/czym-jest-rejestr-pesel)
is a government database containing information about Polish citizens and residents.
Some data from the register are publicly available – including statistics about how many people bear any given name.

So [I analysed this public data set](https://zaimki.pl/blog/imiona-unisex-pesel)
to see which names are used by people with both “male” and “female” legal sex markers.
Admittedly, simply because that's the case does not necessarily mean the name is commonly considered “unisex”,
but it was still a fun and informative analysis. It can also be a useful guide to picking a name
and an argument to be used when applying for a legal name change.

But speaking of guides to picking a new name – we also created exactly that:
[a dictionary of gender-neutral names](https://zaimki.pl/imiona).
It's still not as complete as we would wish, so we haven't added it to other languages yet,
but it's already a very helpful knowledge base.

## Recommendations for translators, creators of questionnaires, and medical personel

We're getting asked regularly to weigh in on some dilemmas or questions that people have about the nonbinary
and gender-neutral language. Some researchers were asking how to prepare the demographic questionnaire
in order to be inclusive of people of all genders. So our team discussed the common pitfalls we're seeing in such forms
and we've prepared [a recommendation](https://zaimki.pl/formularze) (which would actually also be useful in English,
I should get around to translating it whenever I have a moment 😅).
Later it was followed by [a blog post](https://zaimki.pl/blog/formularze-stan-cywilny)
regarding questions of marital status.

Translators were asking for advice regarding the choice of Polish forms that would most closely resemble
the original choice of (neo)pronouns – so we prepared [a guide for translators](https://zaimki.pl/t%C5%82umaczenie).

A friend GP is also the editor of a magazine for doctors and he asked us to prepare a guide for them;
and here it is: [part 1](https://zaimki.pl/blog/poradnik-lekarski-1) and [part 2](https://zaimki.pl/blog/poradnik-lekarski-2).

## Impact on the academia

We maintain [a page listing academic sources](https://zaimki.pl/akademia) and statements by scholars regarding
nonbinary, inclusive and gender-neutral language. It's not massive, 
but the subject is already gaining some interest in the academic circles,
and our project specifically is having an impact too.
There are papers on the subject, studies, people mention us in their theses and quote us and our Census data as sources.

## Spellchecker – ISJP

It's one thing when a computer or a smartphone adds a little red squiggle under the words that you use to describe yourself
– even if you only use those forms that are considered by experts to be “grammatically correct”, albeit niche.
But it gets very annoying when the spellchecker decides to just arbitrarily assign you a gender.
Yet that's what happens to thousands of Polish-speaking enbies every day.

Teaching your spellchecker a new word is simple – but here it's a question of an entire category of words!
The solution for that must come from the developers, not individual users…
So we've started [a (bilingual) petition](https://zaimki.pl/autokorekta) to software companies and contributors
to fix that bug.

Yes, a bug. We're not even asking for a new feature, the petition is only about the most basic case,
the neuter 1<sup>st</sup> and 2<sup>nd</sup> person past tense verbs – forms that are considered
by the [Polish Language Council](https://en.wikipedia.org/wiki/Polish_Language_Council)
to be “correct and consistent with the language system”, forms that have been used by poets centuries ago
and noted in grammar books.

Unfortunately, so far that hasn't yielded any results… But we didn't stop there.
I learned the basics of the Hunspell format and wrote a script that extends an open-source spellcheck dictionary
to include the missing forms.
The result is [ISJP](https://isjp.pl/en/) (this website is bilingual too)
and  it consists of two dictionaries: normative and neological. 

Unfortunately, the subject of spellchecker dictionaries isn't easy…
I'm not experienced in writing this kind of code, testing it is a struggle, and so is even trying to use it…
Every app that uses a spellchecker requires a separate and different process to install a custom dictionary
– if it even allows it at all. And trying to get “the big guys” to support it out of the box is not something
I have time, energy and network to do…

Still, even if its practical uses are limited, I'm very proud of this project and I'm glad that it exists,
even if mostly as a statement. And, of course, it's always open for someone with more expertise to swoop in
and help out 😉 

## Summing up…

When a queer student association from the Jagiellonian University invited us to an expert panel,
I felt uneasy about being called experts at first. After all, we're just a bunch of queers running a website, right?
Sure, language is our passion, some of us have formal training in this subject,
and one of us is even getting a PhD – but still, there must be someone more qualified than us, right?

But here's a thing – there doesn't seem to be… Yes, there are some very respected linguists friendly to our cause,
but there's also the other dimension to this area of linguistics: the experience of being a nonbinary person
who actually needs this language and uses it in their everyday life.
When an enby, or a translator, or an editor, or an educator, or anyone else who cares about
proper representation and inclusion of nonbinary identities in their language,
when they have a question about the subject, they don't ask prof. Miodek, or prof. Bańko, or the [RJP](https://en.wikipedia.org/wiki/Polish_Language_Council) –
they pop _us_ an email. It's absolutely bananas.
It's a subject niche enough that we, a humble bunch of queers, rose to the ranks of experts on it;
but simultaneously a subject so important and quickly growing and evolving that academia, media and the public take notice.

Running this project is not just about coding a platform for cards and adding entries to a bunch of dictionaries.
Zaimki.pl let me do so many things that I never thought I'd ever be doing and enjoying.
And I'll be forever grateful to the universe for the opportunity to be in the middle of this amazing
linguistical nonbinary revolution ❤️
