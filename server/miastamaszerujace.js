require('../src/dotenv')();

const fetch = require('node-fetch');
const { JSDOM } = require("jsdom");
const { Day } = require('../src/calendar/helpers');
const fs = require('fs');
const mailer = require('../src/mailer');
const { createEvents } = require('ics');

const year = Day.today().year;

const fetchEvents = async () => {
    const events = [];

    const html = new JSDOM(await (await fetch('https://miastamaszerujace.pl/')).text());
    const eventsImgs = html.window.document.querySelectorAll('img[src="https://miastamaszerujace.pl/wp-content/uploads/2021/11/Zasob-6@4x.png"]');
    for (let eventImg of eventsImgs) {
        const p = eventImg.closest('p');
        if (p.textContent.includes('Daty kolejnych')) { continue; }

        const [day, month] = p.querySelector('b,strong').textContent.trim().split('/');
        const date = new Day(year, month, day);

        const name = [...p.childNodes].filter(c => c.nodeType === 3 /* text node */).map(c => c.textContent.trim()).filter(t => !!t).join(' ');

        const link = p.querySelector('a').getAttribute('href');

        events.push({
            name,
            date,
            link,
        });
    }

    return events;
}

(async () => {
    const events = await fetchEvents();
    console.log(events);

    const dir = `${__dirname}/../static/calendar`;
    const path = `${dir}/miastamaszerujace-${year}.json`;
    const previous = fs.existsSync(path) ? JSON.parse(fs.readFileSync(path).toString('UTF-8')) : [];
    if (JSON.stringify(events) !== JSON.stringify(previous)) {
        console.log('wykryto zmiany, wysyłam maila');
        mailer('andrea@pronouns.page', 'miastamaszerujace', {
            before: JSON.stringify(previous, null, 4),
            after: JSON.stringify(events, null, 4),
            maxwidth: '100%',
        })
    }
    if (previous.length > events.length) { return; }
    fs.writeFileSync(path, JSON.stringify(events, null, 4));

    createEvents(
        events.map((e, i) => {
            return {
                title: e.name,
                start: [e.date.year, e.date.month, e.date.day],
                end: [e.date.year, e.date.month, e.date.day],
                calName: `Marsze Równości ${year}`,
                sequence: i,
            }
        }),
        (error, value) => {
            if (error) {
                console.error(error);
                return;
            }

            fs.writeFileSync(`${dir}/miastamaszerujace.ics`, value);
        }
    );
})();
