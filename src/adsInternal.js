export const adsInternal = [
    {
        image: 'shop-vt.png',
        placeholders: ['aside-left-middle'],
        link: 'https://shop.pronouns.page',
        display: 'd-none d-md-block',
        alt: 'Pronouns Page merch is here! Get yours at shop.pronouns.page or Etsy!',
    },
    {
        image: 'shop-hz.png',
        placeholders: ['aside-right-top'],
        link: 'https://shop.pronouns.page',
        display: 'd-block d-lg-none',
        alt: 'Pronouns Page merch is here! Get yours at shop.pronouns.page or Etsy!',
    },
];

export const adsInternalPerPlaceholder = {};
for (let ad of adsInternal) {
    for (let placeholder of ad.placeholders) {
        if (!adsInternalPerPlaceholder.hasOwnProperty(placeholder)) {
            adsInternalPerPlaceholder[placeholder] = [];
        }
        adsInternalPerPlaceholder[placeholder].push(ad);
    }
}
